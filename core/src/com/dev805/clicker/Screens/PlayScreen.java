package com.dev805.clicker.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Container;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.dev805.clicker.ClickerGame;
import com.dev805.clicker.Items.Button;
import com.dev805.clicker.Player.Player;

import java.util.Random;

/**
 * Created by Kamalov on 15.11.2015.
 */
public class PlayScreen implements Screen {
    ClickerGame game;
    int temp;
    Stage stage;
    Player pl1;
    Player pl2;
    Random randW;
    Random randH;
    Button btn;
    //Label lbl;
    Label pointslbl;
    //Label lbl1;
    Label pointslbl2;
    Image devisionLine;
    Container l;
    Container lp;
    Skin uiSkin;
    static final int SPEED=400;

    public PlayScreen(ClickerGame game) {
        this.game=game;
        temp=0;
        stage =new Stage();
        pl1=new Player();
        pl2=new Player();
        randW=new Random();
        randH=new Random();
        uiSkin = new Skin(Gdx.files.internal("uiskin.json"));

        //lbl=new Label("Points: ",uiSkin);
        //lbl.setPosition(Gdx.graphics.getWidth()/2-lbl.getWidth()/2,Gdx.graphics.getHeight()/2-lbl.getHeight());
        pointslbl = new Label("0",uiSkin);
        pointslbl.setPosition(Gdx.graphics.getWidth()/2-pointslbl.getWidth()/2,Gdx.graphics.getHeight()/2-pointslbl.getHeight());

        //lbl1=new Label("Points",uiSkin);
        //lbl1.setPosition(Gdx.graphics.getWidth() / 2 - lbl1.getWidth() / 2, Gdx.graphics.getHeight() / 2 + lbl1.getHeight() );
        pointslbl2 = new Label("0",uiSkin);
        //pointslbl2.setPosition(Gdx.graphics.getWidth() / 2 - lbl1.getWidth() / 2 + lbl1.getWidth() + pointslbl.getWidth() / 2, Gdx.graphics.getHeight() / 2 + lbl1.getHeight());


        lp = new Container(pointslbl2);
        lp.setTransform(true);
        lp.addAction(Actions.rotateTo(180));
        lp.setPosition(Gdx.graphics.getWidth() / 2 + pointslbl.getWidth()/2 , Gdx.graphics.getHeight() / 2 + pointslbl2.getHeight());
        //l.addAction(Actions.sequence(Actions.scaleTo(2, 2, 2), Actions.moveTo(10, 10, 3), Actions.color(Color.RED, 6), Actions.delay(0.5f), Actions.rotateTo(180, 5)));


        devisionLine=new Image(uiSkin,"default-rect");
        devisionLine.setPosition(0, Gdx.graphics.getHeight() / 2 + devisionLine.getHeight() / 2);
        devisionLine.setWidth(Gdx.graphics.getWidth());

        stage.addActor(devisionLine);
        stage.addActor(pointslbl);
        stage.addActor(lp);
        stage.act();


    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor( 1, 0, 0, 1 );
        Gdx.gl.glClear( GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT );
        update(delta);
        game.batch.begin();
        stage.draw();

        game.batch.end();
    }

    void update(float dt){
        temp++;
        if(temp>SPEED){
            temp=0;
            stage=new Stage();
            devisionLine.setPosition(0, Gdx.graphics.getHeight() / 2);
            devisionLine.setWidth(Gdx.graphics.getWidth());
            stage.addActor(devisionLine);
            stage.addActor(pointslbl);
            stage.addActor(lp);

            newBtnP1(stage);
            newBtnP2(stage);

        }

        pointslbl.setText(Integer.toString(pl1.score));
        pointslbl2.setText(Integer.toString(pl2.score));
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }


    private void newBtnP1(Stage stage){
        btn=new Button(pl1);
        int torandX=Gdx.graphics.getWidth()-(int)btn.getWidth();
        int torandY=Gdx.graphics.getHeight() / 2 - (int) btn.getHeight();
        btn.setPosition(randW.nextInt(torandX),randH.nextInt(torandY));
        stage.addActor(btn);
        Gdx.input.setInputProcessor(stage);
    }

    private void newBtnP2(Stage stage){
        btn=new Button(pl2);
        int torandX=Gdx.graphics.getWidth()-(int)btn.getWidth();
        int torandY=Gdx.graphics.getHeight()/2-(int)btn.getHeight();
        btn.setPosition(randW.nextInt(torandX), Gdx.graphics.getHeight() / 2 + randH.nextInt(torandY));
        stage.addActor(btn);
        Gdx.input.setInputProcessor(stage);
    }
}
