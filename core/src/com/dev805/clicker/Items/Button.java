package com.dev805.clicker.Items;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.dev805.clicker.Player.Player;

/**
 * Created by Kamalov on 16.11.2015.
 */
public class Button extends Image {
    Player player;
    static String texture1 = "bl1.jpg";
    static String texture2 = "bl2.jpg";
    static int points=100;
    static float size=(float)Math.hypot(Gdx.graphics.getWidth(),Gdx.graphics.getHeight())/10;

    public Button(final Player player) {
        super(new Texture(texture1));
        this.setWidth(size);
        this.setHeight(size);

        this.player=player;
        this.addListener(new InputListener() {
            @Override
            public boolean touchDown (InputEvent event, float x, float y, int pointer, int button) {
                player.score+=points;
                setDrawable(new SpriteDrawable(new Sprite(new Texture(texture2))));
                addAction(Actions.sequence(Actions.alpha(0), Actions.fadeOut(1000)));
                act(Gdx.graphics.getDeltaTime());
                return false;
            }
            @Override
            public void touchUp (InputEvent event, float x, float y, int pointer, int button) {
            }
        });
    }

}
