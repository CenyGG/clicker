package com.dev805.clicker.Hud;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Disposable;
import com.dev805.clicker.ClickerGame;

/**
 * Created by Kamalov on 15.11.2015.
 */
public class Hud implements Disposable {
    Stage stage;
    ClickerGame game;
    int player1Points=0;
    int player2Points=0;
    Label label1Points;
    Label label2Points;
    Label label1Text;
    Label label2Text;

    public Hud(ClickerGame game) {
        this.game=game;

    }

    public void draw(){
        stage.draw();
    }

    public void update(){

    }

    @Override
    public void dispose() {

    }

    public void incPlayer1Points(int val){
        player1Points+=val;
    }
    public void incPlayer2Points(int val){
        player2Points+=val;
    }
}
